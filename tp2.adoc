= Bases de datos 1: Trabajo Práctico 2
Juan Grenz <j.grenz@hotmail.com>; Martin Bornholdt <mbornholdt3@gmail.com>; Matias Dutra <matydu98@gmail.com>
{docdate}. Docentes Daniel Rondelli y Mariano Trigila (COM-01)
:numbered:
:source-highlighter: coderay
:tabsize: 4

== Introducción

El presente trabajo consiste en diseñar un modelo de datos relativos a
operaciones de tarjetas de crédito. En comparación con el primer tp, en
esta parte se añaden funcionalidades y alertas a la base de datos.

== Descripción

=== Dificultades y soluciones

Eliminar Constraint::
	El enunciado nos pedía que le usuarie tenga la posibilidad de borrar todas
	las PK's y FK's. Al principio pusimos que se eliminen (`drop`) las PK's y luego
	las FK's. Esto traía problemas debido a que al eliminar primero las PK's, las FK's
	ya no tenían referencias a las tablas y era imposible borrarlas. Se solucionó
	invirtiendo el orden, es decir, primero se eliminan las FK's y luego las PK's.

Crear cierres de tarjetas::
	Tuvimos el problema que no podíamos armar las fechas que necesitábamos
	para comparar entre ellas para la creación de los cierres. Lo solucionamos casteando
	el valor como `date` utilizando `valor::date`.
	Otro problema fue que queríamos comparar las fechas hardcodeando el código utilizando
	condicionales. Evitamos esto utilizando `INTERVAL`.

Rechazo de compra por monto superado::
	Nos encontramos con el problema de que al querer comparar el monto total
	de las compras realizadas por una tarjeta, si la tarjeta no posee ninguna compra
	asociada, el monto total era NULL y por lo tanto, la condición fallaba con la primer compra.
	Se soluciono agregando un condicional que si el monto total es NULL, cambiarlo por
	el valor 0.

=== Descripción general del programa

- El programa cuenta con funciones que se encargan de
	1. La creación de las tablas
	2. La creación y eliminación de PK's y FK's
	3. La inserción de datos en las tablas
	4. Creación de alertas, es decir, `triggers`
	5. La autorización de una compra
	6. La generación de un resumen de una tarjeta
	7. Un test para los consumos

== Implementación

=== Implementación Go y SQL

[source, go]
----
package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

var crear_tablas = `CREATE TABLE cliente (
		nrocliente int,
		nombre text,
		apellido text,
		direccion text,
		telefono char(12)
	);

	CREATE TABLE tarjeta (
		nrotarjeta char(16),
		nrocliente int,
		validadesde char(6), --e.g. 201106
		validahasta char(6),
		codseguridad char(4),
		limitecompra decimal(8,2),
		estado char(10) --"vigente", "suspendida", "anulada"
	);

	CREATE TABLE comercio (
		nrocomercio int,
		nombre text,
		domicilio text,
		codigopostal char(8),
		telefono char(12)
	);

	CREATE TABLE compra (
		nrooperacion int,
		nrotarjeta char(16),
		nrocomercio int,
		fecha timestamp,
		monto decimal(7,2),
		pagado boolean
	);

	CREATE TABLE rechazo (
		nrorechazo serial,
		nrotarjeta char(16),
		nrocomercio int,
		fecha timestamp,
		monto decimal(7,2),
		motivo text
	);

	CREATE TABLE cierre (
		año int,
		mes int,
		terminacion int,
		fechainicio date,
		fechacierre date,
		fechavto date
	);

	CREATE TABLE cabecera (
		nroresumen int,
		nombre text,
		apellido text,
		domicilio text,
		nrotarjeta char(16),
		desde date,
		hasta date,
		vence date,
		total decimal(8,2)
	);

	CREATE TABLE detalle (
		nroresumen int,
		nrolinea int,
		fecha date,
		nombrecomercio text,
		monto decimal(7,2)
	);

	CREATE TABLE alerta (
		nroalerta serial,
		nrotarjeta char(16),
		fecha timestamp,
		nrorechazo int,
		codalerta int, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
		descripcion text
	);

	CREATE TABLE consumo (
		nrotarjeta char(16),
		codseguridad char(4),
		nrocomercio int,
		monto decimal(7,2)
	);
`

var crear_keys = `alter table cliente add constraint cliente_pk primary key (nrocliente);
alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
alter table comercio add constraint comercio_pk primary key (nrocomercio);
alter table compra add constraint compra_pk primary key (nrooperacion);
alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
alter table cierre add constraint cierre_pk primary key (año,mes,terminacion);
alter table cabecera add constraint cabecera_pk primary key (nroresumen);
alter table detalle add constraint detalle_pk primary key (nroresumen,nrolinea);
alter table alerta add constraint alerta_pk primary key (nroalerta);

alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) 
references cliente (nrocliente);

alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta)
references tarjeta (nrotarjeta);
alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio)
references comercio (nrocomercio);

alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio)
references comercio (nrocomercio); 

alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta)
references tarjeta (nrotarjeta);
`

var crear_datos = `insert into cliente values (1,'Tony','Stark','AV. FRANCISCO BEIRÓ 3146','15-1231-8706');
insert into cliente values (2,'Peter','Parker','AV. CASEROS 2938','15-1236-8781');
insert into cliente values (3,'Clark','Kent','BELGRANO 3162','15-1405-1649');
insert into cliente values (4,'Bruce','Wayne','SANTA CRUZ 1038','15-8622-8559');
insert into cliente values (5,'Goku','Son','AV. RIVADAVIA 16275','15-3907-1318');
insert into cliente values (6,'Elsa','Popepe','20 DE SEPTIEMBRE 341','15-8630-8703');
insert into cliente values (7,'Bruce','Banner','EMILIO LAMARCA 1965','15-6888-6518');
insert into cliente values (8,'Jack','Sparrow','AV. LISANDRO DE LA TORRE 1151','15-6330-6314');
insert into cliente values (9,'Steve','Rogers','SARMIENTO 3978','15-4636-5995');
insert into cliente values (10,'Thor','Odinson','MANUEL ESTRADA 372','15-7516-1940');
insert into cliente values (11,'Natasha','Romanoff','CAMACUA 5613','15-7458-5748');
insert into cliente values (12,'Barry','Allen','SAN JUSTO 801','15-8148-7481');
insert into cliente values (13,'Homero','Simpson','AV. SIEMPREVIVA 742','15-7761-1995');
insert into cliente values (14,'Vegeta','Saiyan','CALLE FALSA 123','15-6392-1379');
insert into cliente values (15,'Barney','Gomez','25 DE MAYO 151','15-4478-2830');
insert into cliente values (16,'Max','Power','HIDALGO 860','15-7886-6538');
insert into cliente values (17,'Stan', 'Lee','H. YRIGOYEN 997','15-2714-3536');
insert into cliente values (18,'Luke','Skywalker','PARANÁ 6370','15-7466-9047');
insert into cliente values (19,'Han','Solo','AV. CORRIENTES 6180','15-3139-9275');
insert into cliente values (20,'Moe','Szyslak','EVA PERÓN 3349','15-2878-0505');

insert into comercio values (1, 'Mac Dornal', 'Eva Peron 873','B6450XAA','15-6545-1211');
insert into comercio values (2, 'Barger Queen', 'Amenabar 9139','B6450XAC','15-5115-8971');
insert into comercio values (3, 'Hamburguesas Ketchup', 'Romian 510','B6450XAB','15-4741-2997');
insert into comercio values (4, 'Morvistar', 'Rolland 621','B6450XAD','15-4741-2362');
insert into comercio values (5, 'Krusty Burgers', 'Mozart 123','B6450XAF','15-5674-6012');
insert into comercio values (6, 'Mike', 'Carlos Pellegrini 6264','B6407XAB','15-5409-9662');
insert into comercio values (7, 'Adides', 'El Greco 78','B7241XAB','15-9870-1112');
insert into comercio values (8, 'AlFreddo', 'Av. Jose Leon Suarez 104','B6748XAA','15-8956-8741');
insert into comercio values (9, 'Klarin', 'John F. Kennedy 491','B8164XAA','15-6568-4448');
insert into comercio values (10, 'Jennigstone', 'Rawson 489','B8164XAB','15-9595-3264');
insert into comercio values (11, 'Fanasonic', 'Buenos Aires 124','B8164XAC','15-9468-5935');
insert into comercio values (12, 'Sorny', '9 de Julio 907','B8164XAD','15-6870-9934');
insert into comercio values (13, 'Fabrega', '25 de Mayo 2505','B7530XAC','15-9298-8123');
insert into comercio values (14, 'Suwei', 'Pte Juan Domingo Perón 666 ','B7530XAD','15-6859-4512');
insert into comercio values (15, 'Kwik-E-Mart', 'Cabildo 8488','B7530XAH','15-5365-6357');
insert into comercio values (16, 'Moe', 'Av. Santa Fe 8950','B7530XAA','15-1232-4512');
insert into comercio values (17, 'El bar de la UNGS', 'Juan Maria Gutierrez 1150','B7530XAF','15-6682-1224');
insert into comercio values (18, 'Gurbarino', 'Bullrich 1031','B7530XAB','15-9852-6812');
insert into comercio values (19, 'Lacosta', 'Av. Corrientes 9910','B7530XAE','15-9853-1478');
insert into comercio values (20, 'Lecheria miau', 'Cordoba 612','B6708XAB','15-9785-4223');

insert into tarjeta values ('4627633796432790',1,'199401','200601','1234',999999.99,'anulada');
insert into tarjeta values ('4606669094765288',1,'200705','201905','9825',900000.00,'vigente');
insert into tarjeta values ('4309892964905889',2,'200805','202005','5421',1000.00,'vigente');
insert into tarjeta values ('4705179869058384',3,'200804','202004','6832',15000.00,'vigente');
insert into tarjeta values ('4778887665102069',4,'200803','202003','8100',900000.00,'vigente');
insert into tarjeta values ('4720999501775726',4,'201002','202202','6812',70000.00,'vigente');
insert into tarjeta values ('4315285179011207',5,'201001','202201','9865',15000.00,'vigente');
insert into tarjeta values ('4813559786031868',6,'201109','202309','8402',12000.00,'vigente');
insert into tarjeta values ('4562482253184232',7,'201110','202310','6035',1000.00,'vigente');
insert into tarjeta values ('4394485803687176',8,'201111','202311','9822',20000.00,'vigente');
insert into tarjeta values ('4397622918073892',9,'201212','202412','1258',10000.00,'vigente');
insert into tarjeta values ('4263913794226636',10,'201209','202409','1035',4500.00,'vigente');
insert into tarjeta values ('4641381760773711',11,'201208','202408','2610',5000.00,'vigente');
insert into tarjeta values ('4683099390400259',12,'200707','201907','3078',100000.00,'vigente');
insert into tarjeta values ('4988131767657095',13,'200612','201812','9534',80000.00,'vigente');
insert into tarjeta values ('4674630639638823',14,'200711','201911','8026',30000.00,'vigente');
insert into tarjeta values ('4764684936724189',15,'200712','201912','7098',25000.00,'vigente');
insert into tarjeta values ('4626337979547995',16,'200612','201812','6812',12000.00,'vigente');
insert into tarjeta values ('4619408553568483',17,'200808','202008','5456',10000.00,'vigente');
insert into tarjeta values ('4648528090512563',18,'200807','202007','4232',100000.00,'vigente');
insert into tarjeta values ('4444701795160093',19,'200801','202001','3589',50000.00,'vigente');
insert into tarjeta values ('4901474084361461',20,'200702','201902','2018',500.00,'vigente');

CREATE FUNCTION cierre_tarjetas () RETURNS void AS $$
    DECLARE
        mes int;
        terminacion int;
        random int;
        fechaInicio date;
        fechaCierre date;
        fechavto date;
        
    BEGIN
        FOR mes in 1..12 LOOP
            FOR terminacion in 0..9 LOOP
                random := FLOOR(RANDOM()*(15)+2);
                fechaInicio := ('2018-' || mes || '-' || random)::date;
                fechaCierre := fechaInicio + INTERVAL '1 month';
                fechavto    := fechaCierre + INTERVAL '15 days';

                INSERT INTO cierre VALUES(2018, mes, terminacion, fechaInicio, fechaCierre, fechavto);
            END LOOP;
        END LOOP;
    END;
$$ LANGUAGE plpgsql;
`
var eliminar_keys = `alter table tarjeta drop constraint tarjeta_nrocliente_fk;
alter table compra drop constraint compra_nrotarjeta_fk;
alter table compra drop constraint compra_nrocomercio_fk;
alter table rechazo drop constraint rechazo_nrocomercio_fk;
alter table cabecera drop constraint cabecera_nrotarjeta_fk;

alter table cliente drop constraint cliente_pk;
alter table tarjeta drop constraint tarjeta_pk;
alter table comercio drop constraint comercio_pk;
alter table compra drop constraint compra_pk;
alter table rechazo drop constraint rechazo_pk;
alter table cierre drop constraint cierre_pk;
alter table cabecera drop constraint cabecera_pk;
alter table detalle drop constraint detalle_pk;
alter table alerta drop constraint alerta_pk;
`

var SPautorizar_compra = `CREATE FUNCTION autorizar_compra(numerotarj char(16), codseguridad char(4), nrocomercio int, montoingresado int) RETURNS boolean AS $$
	DECLARE 

		verificador record;
		monto_total int;
		fecha_tarjeta date;
		fecha_actual date;

	BEGIN
		SELECT * INTO verificador FROM tarjeta WHERE numerotarj = tarjeta.nrotarjeta;
		IF not FOUND THEN
			INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?tarjeta no válida');
			RETURN false;

		ELSE
			IF verificador.estado != 'vigente' THEN
				INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?tarjeta no vigente');
				RETURN false;

			ELSE
				IF verificador.codseguridad != codseguridad THEN
					INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?código de seguridad inválido');
					RETURN false;

				ELSE
					SELECT SUM (monto) INTO monto_total FROM compra WHERE numerotarj = compra.nrotarjeta and compra.pagado = false;
					IF monto_total ISNULL THEN
						monto_total := 0;
					END IF;
					
					IF monto_total + montoingresado > verificador.limitecompra THEN
						INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?supera límite de tarjeta');
						RETURN false;

					ELSE
						fecha_tarjeta := TO_DATE(verificador.validahasta, 'YYYYMM' );
						fecha_actual := NOW()::DATE;
						IF (fecha_actual >= fecha_tarjeta) THEN
							INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?plazo de vigencia expirado');
							RETURN false;

						ELSE
							SELECT * INTO verificador FROM tarjeta WHERE tarjeta.estado = 'suspendida';
							IF verificador.estado = 'suspendida' THEN
								INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, 'la tarjeta se encuentra suspendida');
								RETURN false;

							ELSE
								INSERT INTO compra VALUES(DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, false);
								RETURN true;
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;

	RETURN false;

	END;
$$ LANGUAGE plpgsql;
`

var SPgenerar_resumen = `CREATE FUNCTION generar_resumen(numerocli int, periodo_año char(4), periodo_mes char(2)) RETURNS void AS $$ 
	DECLARE 

		linea int;
		comercioDet RECORD;
		candidato RECORD;
		t RECORD;
		c RECORD;
		fechas RECORD;

	BEGIN
		SELECT * INTO candidato FROM cliente WHERE numerocli = cliente.nrocliente;

		FOR t IN SELECT * FROM tarjeta WHERE candidato.nrocliente = tarjeta.nrocliente LOOP

			linea := 0;

			SELECT fechainicio,fechacierre,fechavto into fechas from cierre where 
			to_number(RIGHT(t.nrotarjeta, 1),'0') = cierre.terminacion and 
			to_number(periodo_año,'9999') = cierre.año and to_number(periodo_mes, '99') = cierre.mes;

			INSERT INTO cabecera VALUES (DEFAULT, candidato.nombre, candidato.apellido, candidato.direccion, t.nrotarjeta, fechas.fechainicio, fechas.fechacierre, fechas.fechavto);

			FOR c IN SELECT * FROM compra WHERE t.nrotarjeta = compra.nrotarjeta AND (compra.fecha >= fechas.fechainicio AND compra.fecha <= fechas.fechacierre) LOOP

				linea = linea + 1;
				SELECT * INTO comercioDet FROM comercio WHERE c.nrocomercio = comercio.nrocomercio;

				INSERT INTO detalle VALUES( (SELECT MAX(nroresumen) FROM cabecera) , linea, c.fecha, comercioDet.nombre, c.monto);

			END LOOP;

			UPDATE cabecera SET total = (SELECT COUNT(monto) FROM detalle WHERE detalle.nroresumen = (SELECT MAX(nroresumen) FROM cabecera))
			WHERE cabecera.nroresumen = (SELECT MAX(nroresumen) FROM cabecera);

		END LOOP;
	END;
$$ LANGUAGE plpgsql;
`

var trigger_compra_rechazada = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_rechazada() RETURNS TRIGGER AS $$
	BEGIN
		INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), new.nrorechazo, 0, 'rechazo');
		RETURN new;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_compra_rechazada 
	AFTER 
	INSERT ON rechazo
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_rechazada();
`

var trigger_compra_unminuto = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_unminuto() RETURNS TRIGGER AS $$
	DECLARE
		anterior record;
		codpostal_nueva record;
		codpostal_anterior record;
		
	BEGIN

		SELECT * INTO anterior FROM compra WHERE new.nrotarjeta = compra.nrotarjeta and 
		compra.nrooperacion = (SELECT MAX (nrooperacion) FROM compra);

		SELECT codigopostal INTO codpostal_nueva FROM comercio WHERE new.nrocomercio = comercio.nrocomercio;
		SELECT codigopostal INTO codpostal_anterior FROM comercio WHERE anterior.nrocomercio = comercio.nrocomercio;

		IF FOUND THEN
			IF (((new.fecha - anterior.fecha) < INTERVAL '1 min')) and (codpostal_nueva = codpostal_anterior) THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), NULL, 1, 'compra 1min');
			END IF;
		END IF;

		RETURN new;

	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_compra_unminuto
	BEFORE
	INSERT ON compra
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_unminuto();
`

var trigger_compra_cincominutos = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_cincominutos() RETURNS TRIGGER AS $$
	DECLARE

		anterior record;
		codpostal_nueva record;
		codpostal_anterior record;
		
	BEGIN

		SELECT * INTO anterior FROM compra WHERE new.nrotarjeta = compra.nrotarjeta and 
		compra.nrooperacion = (SELECT MAX (nrooperacion) FROM compra);

		SELECT codigopostal INTO codpostal_nueva FROM comercio WHERE new.nrocomercio = comercio.nrocomercio;
		SELECT codigopostal INTO codpostal_anterior FROM comercio WHERE anterior.nrocomercio = comercio.nrocomercio;

		IF FOUND THEN
			IF (((new.fecha - anterior.fecha) < INTERVAL '5 mins')) and (codpostal_nueva != codpostal_anterior) THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), NULL, 5, 'compra 5min');
			END IF;
		END IF;

		RETURN new;

	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_compra_cincominutos
	BEFORE
	INSERT ON compra
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_cincominutos();
`

var trigger_compra_dosrechazos = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_dosrechazos() RETURNS TRIGGER AS $$
	DECLARE

		rechazos INT;

	BEGIN
		IF new.motivo = '?supera límite de tarjeta' THEN
			SELECT COUNT (*) INTO rechazos FROM rechazo WHERE new.nrotarjeta = rechazo.nrotarjeta and 
															new.fecha::date = rechazo.fecha::date
															and rechazo.motivo = '?supera límite de tarjeta';
			IF rechazos = 2 THEN
				UPDATE tarjeta SET estado = 'suspendida' WHERE new.nrotarjeta = tarjeta.nrotarjeta;
				
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), new.nrorechazo, 32, 'limite');
			END IF;
		END IF;

		RETURN new;
	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_compra_dosrechazos
	AFTER
	INSERT ON rechazo
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_dosrechazos();
`

var probar_consumo = `CREATE FUNCTION prueba_consumo(nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto int) RETURNS void AS $$
	DECLARE

		autorizado boolean;

	BEGIN
		SELECT autorizar_compra(nrotarjeta, codseguridad, nrocomercio, monto) INTO autorizado;
			IF autorizado = true THEN
				raise notice 'Compra aceptada';
			ELSE
				raise notice 'Compra rechazada';
			END IF;
	END;
$$ LANGUAGE plpgsql;
`

func createDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	db.Exec("drop database banco")
	_, err = db.Exec(`create database banco`)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()
}

func iniciar_tablas(db *sql.DB) {
	_, err := db.Exec(crear_tablas)
	if err != nil {
		log.Fatal(err)
	}
}

func iniciar_keys(db *sql.DB) {
	_, err := db.Exec(crear_keys)
	if err != nil {
		log.Fatal(err)
	}
}

func iniciar_datos(db *sql.DB) {
	_, err := db.Exec(crear_datos)
	if err != nil {
		log.Fatal(err)
	}
}

func borrar_keys(db *sql.DB) {
	_, err := db.Exec(eliminar_keys)
	if err != nil {
		log.Fatal(err)
	}
}

func autorizar_compra(db *sql.DB) {
	_, err := db.Exec(SPautorizar_compra)
	if err != nil {
		log.Fatal(err)
	}
}

func generar_resumen(db *sql.DB) {
	_, err := db.Exec(SPgenerar_resumen)
	if err != nil {
		log.Fatal(err)
	}
}

/*func trigger_rechazada(db *sql.DB) {
	_, err := db.Exec(trigger_compra_rechazada)
	if err != nil {
		log.Fatal(err)
	}
}

func trigger_1minuto(db *sql.DB) {
	_, err := db.Exec(trigger_compra_unminuto)
	if err != nil {
		log.Fatal(err)
	}
}

func trigger_5minutos(db *sql.DB) {
	_, err := db.Exec(trigger_compra_cincominutos)
	if err != nil {
		log.Fatal(err)
	}
}

func trigger_2rechazos(db *sql.DB) {
	_, err := db.Exec(trigger_compra_dosrechazos)
	if err != nil {
		log.Fatal(err)
	}
}*/

func prueba_consumo(db *sql.DB) {
	_, err := db.Exec(probar_consumo)
	if err != nil {
		log.Fatal(err)
	}
}

func triggers(db *sql.DB) {
	_, err := db.Exec(trigger_compra_rechazada)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_unminuto)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_cincominutos)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_dosrechazos)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	createDatabase()

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=banco sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var opcionElegida string
	check := 0

	for check < 1 {
		fmt.Printf("1. Crear tablas \n")
		fmt.Printf("2. Crear PK y FK \n")
		fmt.Printf("3. Insertar datos a las tablas \n")
		fmt.Printf("4. Eliminar PK y FK de las tablas \n")
		fmt.Printf("5. Autorizar compra \n")
		fmt.Printf("6. Crear triggers \n")
		fmt.Printf("7. Probar consumo \n")
		fmt.Printf("0. Salir. \n")

		fmt.Printf("\nOpcion elegida: ")
		fmt.Scanf("%s", &opcionElegida)

		if opcionElegida == "1" {
			iniciar_tablas(db)
			fmt.Printf("Tablas creadas! \n\n")
		} else if opcionElegida == "2" {
			iniciar_keys(db)
			fmt.Printf("PK y FK agregadas a las tablas! \n\n")
		} else if opcionElegida == "3" {
			iniciar_datos(db)
			fmt.Printf("Datos ingresados a las tablas! \n\n")
		} else if opcionElegida == "4" {
			borrar_keys(db)
			fmt.Printf("PK y FK borradas de las tablas. \n\n")
		} else if opcionElegida == "5" {
			autorizar_compra(db)
		} else if opcionElegida == "6" {
			triggers(db)
		} else if opcionElegida == "7" {
			prueba_consumo(db)
		} else if opcionElegida == "0" {
			fmt.Printf("Gracias. Vuelva prontos! \n\n")
			check = 1
		} else {
			fmt.Printf("Opcion incorrecta! Elija nuevamente por favor.\n\n")
		}
	}
}

----

=== Implementación Go de una base de datos NoSQL basada en JSON

[source, go]
----
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	bolt "github.com/coreos/bbolt"
)

type Cliente struct {
	Nrocliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}
type Tarjeta struct {
	Nrotarjeta   int
	Nrocliente   int
	Validadesde  string
	Validahasta  string
	Codseguridad string
	Limitecompra float64
	Estado       string
}
type Comercio struct {
	Nrocomercio  int
	Nombre       string
	Domicilio    string
	Codigopostal string
	Telefono     string
}
type Compra struct {
	Nrooperacion int
	Nrotarjeta   int
	Nrocomercio  int
	Fecha        time.Time
	Monto        float64
	Pagado       bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {
	var opcionElegida string
	check := 0

	for check < 1 {
		fmt.Printf("1. Clientes NoSQL \n")
		fmt.Printf("2. Tarjetas NoSQL \n")
		fmt.Printf("3. Comercios NoSQL \n")
		fmt.Printf("4. Compras NoSQL \n")
		fmt.Printf("0. Salir. \n")

		fmt.Printf("\nOpcion elegida: ")
		fmt.Scanf("%s", &opcionElegida)

		if opcionElegida == "1" {
			clientes()
			fmt.Printf("Tabla Clientes cargada! \n\n")
		} else if opcionElegida == "2" {
			tarjetas()
			fmt.Printf("Tabla Tarjetas cargada! \n\n")
		} else if opcionElegida == "3" {
			comercios()
			fmt.Printf("Tabla Comercios cargada! \n\n")
		} else if opcionElegida == "4" {
			compras()
			fmt.Printf("Tabla Compras cargada! \n\n")
		} else if opcionElegida == "0" {
			fmt.Printf("Gracias. Vuelva prontos! \n\n")
			check = 1
		} else {
			fmt.Printf("Opcion incorrecta! Elija nuevamente por favor.\n\n")
		}
	}

}

func clientes() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	cliente := Cliente{8, "Jack", "Sparrow", "AV. LISANDRO DE LA TORRE 1151", "15-6330-6314"}
	data, err := json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)

	cliente = Cliente{13, "Homero", "Simpson", "AV. SIEMPREVIVA 742", "15-7761-1995"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)

	cliente = Cliente{18, "Luke", "Skywalker", "PARANÁ 6370", "15-7466-9047"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)
}

func tarjetas() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tarjeta := Tarjeta{4394485803687176, 8, "201111", "202311", "9822", 20000.00, "vigente"}
	data, err := json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err := ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)

	tarjeta = Tarjeta{4988131767657095, 13, "200612", "201812", "9534", 80000.00, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)

	tarjeta = Tarjeta{4648528090512563, 18, "200807", "202007", "4232", 100000.00, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)
}

func comercios() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	comercio := Comercio{3, "Hamburguesas Ketchup", "Romian 510", "B6450XAB", "15-4741-2997"}
	data, err := json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

	comercio = Comercio{12, "Sorny", "9 de Julio 907", "B8164XAD", "15-6870-9934"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

	comercio = Comercio{13, "Fabrega", "25 de Mayo 2505", "B7530XAC", "15-9298-8123"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

}

func compras() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	t := time.Date(2018, time.July, 3, 16, 30, 0, 0, time.UTC)
	compra := Compra{1, 4394485803687176, 3, t.Local(), 1500.00, true}
	data, err := json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err := ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

	t = time.Date(2018, time.October, 26, 22, 15, 35, 0, time.UTC)
	compra = Compra{2, 4394485803687176, 12, t.Local(), 240.50, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

	t = time.Date(2018, time.May, 13, 9, 45, 21, 0, time.UTC)
	compra = Compra{3, 4394485803687176, 13, t.Local(), 600.50, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

}

----
== Conclusiones
 
Con lo que nos pedía esta segunda parte del trabajo, pudimos poner en
práctica la implementación de triggers y de stored procedures en una
base de datos.

También conocimos el lenguaje *Go* el cual pusimos en
práctica generando un menú que ejecute funciones que contenía código
SQL. 

Además, conocimos la base de datos NoSQL con implementación JSON.
