package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

var crear_tablas = `CREATE TABLE cliente (
		nrocliente serial,
		nombre text,
		apellido text,
		direccion text,
		telefono char(12)
	);

	CREATE TABLE tarjeta (
		nrotarjeta char(16),
		nrocliente int,
		validadesde char(6), --e.g. 201106
		validahasta char(6),
		codseguridad char(4),
		limitecompra decimal(8,2),
		estado char(10) --'vigente', 'suspendida', 'anulada'
	);

	CREATE TABLE comercio (
		nrocomercio serial,
		nombre text,
		domicilio text,
		codigopostal char(8),
		telefono char(12)
	);

	CREATE TABLE compra (
		nrooperacion serial,
		nrotarjeta char(16),
		nrocomercio int,
		fecha timestamp,
		monto decimal(7,2),
		pagado boolean
	);

	CREATE TABLE rechazo (
		nrorechazo serial,
		nrotarjeta char(16),
		nrocomercio int,
		fecha timestamp,
		monto decimal(7,2),
		motivo text
	);

	CREATE TABLE cierre (
		año int,
		mes int,
		terminacion int,
		fechainicio date,
		fechacierre date,
		fechavto date
	);

	CREATE TABLE cabecera (
		nroresumen serial,
		nombre text,
		apellido text,
		domicilio text,
		nrotarjeta char(16),
		desde date,
		hasta date,
		vence date,
		total decimal(8,2)
	);

	CREATE TABLE detalle (
		nroresumen int,
		nrolinea int,
		fecha date,
		nombrecomercio text,
		monto decimal(7,2)
	);

	CREATE TABLE alerta (
		nroalerta serial,
		nrotarjeta char(16),
		fecha timestamp,
		nrorechazo int,
		codalerta int, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
		descripcion text
	);

	CREATE TABLE consumo (
		nrotarjeta char(16),
		codseguridad char(4),
		nrocomercio int,
		monto decimal(7,2)
	);
`

var crear_keys = `alter table cliente add constraint cliente_pk primary key (nrocliente);
	alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
	alter table comercio add constraint comercio_pk primary key (nrocomercio);
	alter table compra add constraint compra_pk primary key (nrooperacion);
	alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
	alter table cierre add constraint cierre_pk primary key (año,mes,terminacion);
	alter table cabecera add constraint cabecera_pk primary key (nroresumen);
	alter table detalle add constraint detalle_pk primary key (nroresumen,nrolinea);
	alter table alerta add constraint alerta_pk primary key (nroalerta);

	alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) 
	references cliente (nrocliente);

	alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta)
	references tarjeta (nrotarjeta);
	alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio)
	references comercio (nrocomercio);

	alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio)
	references comercio (nrocomercio); 

	alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta)
	references tarjeta (nrotarjeta); 
`

var crear_datos = `INSERT INTO cliente VALUES ( DEFAULT, 'Tony','Stark','AV. FRANCISCO BEIRÓ 3146','15-1231-8706');
	INSERT INTO cliente VALUES ( DEFAULT, 'Peter','Parker','AV. CASEROS 2938','15-1236-8781');
	INSERT INTO cliente VALUES ( DEFAULT, 'Clark','Kent','BELGRANO 3162','15-1405-1649');
	INSERT INTO cliente VALUES ( DEFAULT, 'Bruce','Wayne','SANTA CRUZ 1038','15-8622-8559');
	INSERT INTO cliente VALUES ( DEFAULT, 'Goku','Son','AV. RIVADAVIA 16275','15-3907-1318');
	INSERT INTO cliente VALUES ( DEFAULT, 'Elsa','Popepe','20 DE SEPTIEMBRE 341','15-8630-8703');
	INSERT INTO cliente VALUES ( DEFAULT, 'Bruce','Banner','EMILIO LAMARCA 1965','15-6888-6518');
	INSERT INTO cliente VALUES ( DEFAULT, 'Jack','Sparrow','AV. LISANDRO DE LA TORRE 1151','15-6330-6314');
	INSERT INTO cliente VALUES ( DEFAULT, 'Steve','Rogers','SARMIENTO 3978','15-4636-5995');
	INSERT INTO cliente VALUES ( DEFAULT, 'Thor','Odinson','MANUEL ESTRADA 372','15-7516-1940');
	INSERT INTO cliente VALUES ( DEFAULT, 'Natasha','Romanoff','CAMACUA 5613','15-7458-5748');
	INSERT INTO cliente VALUES ( DEFAULT, 'Barry','Allen','SAN JUSTO 801','15-8148-7481');
	INSERT INTO cliente VALUES ( DEFAULT, 'Homero','Simpson','AV. SIEMPREVIVA 742','15-7761-1995');
	INSERT INTO cliente VALUES ( DEFAULT, 'Vegeta','Saiyan','CALLE FALSA 123','15-6392-1379');
	INSERT INTO cliente VALUES ( DEFAULT, 'Barney','Gomez','25 DE MAYO 151','15-4478-2830');
	INSERT INTO cliente VALUES ( DEFAULT, 'Max','Power','HIDALGO 860','15-7886-6538');
	INSERT INTO cliente VALUES ( DEFAULT, 'Stan', 'Lee','H. YRIGOYEN 997','15-2714-3536');
	INSERT INTO cliente VALUES ( DEFAULT, 'Luke','Skywalker','PARANÁ 6370','15-7466-9047');
	INSERT INTO cliente VALUES ( DEFAULT, 'Han','Solo','AV. CORRIENTES 6180','15-3139-9275');
	INSERT INTO cliente VALUES ( DEFAULT, 'Moe','Szyslak','EVA PERÓN 3349','15-2878-0505');

	INSERT INTO comercio VALUES ( DEFAULT, 'Mac Dornal', 'Eva Peron 873','B6450XAA','15-6545-1211');
	INSERT INTO comercio VALUES ( DEFAULT, 'Barger Queen', 'Amenabar 9139','B6450XAC','15-5115-8971');
	INSERT INTO comercio VALUES ( DEFAULT, 'Hamburguesas Ketchup', 'Romian 510','B6450XAB','15-4741-2997');
	INSERT INTO comercio VALUES ( DEFAULT, 'Morvistar', 'Rolland 621','B6450XAD','15-4741-2362');
	INSERT INTO comercio VALUES ( DEFAULT, 'Krusty Burgers', 'Mozart 123','B6407XAB','15-5674-6012');
	INSERT INTO comercio VALUES ( DEFAULT, 'Mike', 'Carlos Pellegrini 6264','B6407XAB','15-5409-9662');
	INSERT INTO comercio VALUES ( DEFAULT, 'Adides', 'El Greco 78','B7241XAB','15-9870-1112');
	INSERT INTO comercio VALUES ( DEFAULT, 'AlFreddo', 'Av. Jose Leon Suarez 104','B6748XAA','15-8956-8741');
	INSERT INTO comercio VALUES ( DEFAULT, 'Klarin', 'John F. Kennedy 491','B8164XAA','15-6568-4448');
	INSERT INTO comercio VALUES ( DEFAULT, 'Jennigstone', 'Rawson 489','B8164XAB','15-9595-3264');
	INSERT INTO comercio VALUES ( DEFAULT, 'Fanasonic', 'Buenos Aires 124','B8164XAC','15-9468-5935');
	INSERT INTO comercio VALUES ( DEFAULT, 'Sorny', '9 de Julio 907','B8164XAD','15-6870-9934');
	INSERT INTO comercio VALUES ( DEFAULT, 'Fabrega', '25 de Mayo 2505','B7530XAC','15-9298-8123');
	INSERT INTO comercio VALUES ( DEFAULT, 'Suwei', 'Pte Juan Domingo Perón 666 ','B7530XAD','15-6859-4512');
	INSERT INTO comercio VALUES ( DEFAULT, 'Kwik-E-Mart', 'Cabildo 8488','B7530XAH','15-5365-6357');
	INSERT INTO comercio VALUES ( DEFAULT, 'Moe', 'Av. Santa Fe 8950','B7530XAA','15-1232-4512');
	INSERT INTO comercio VALUES ( DEFAULT, 'El bar de la UNGS', 'Juan Maria Gutierrez 1150','B7530XAF','15-6682-1224');
	INSERT INTO comercio VALUES ( DEFAULT, 'Gurbarino', 'Bullrich 1031','B7530XAB','15-9852-6812');
	INSERT INTO comercio VALUES ( DEFAULT, 'Lacosta', 'Av. Corrientes 9910','B7530XAE','15-9853-1478');
	INSERT INTO comercio VALUES ( DEFAULT, 'Lecheria miau', 'Cordoba 612','B6708XAB','15-9785-4223');

	INSERT INTO tarjeta VALUES ('4627633796432790',1,'199401','200601','1234',999999.99,'anulada');
	INSERT INTO tarjeta VALUES ('4606669094765288',1,'200705','201911','9825',900000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4309892964905889',2,'200805','202005','5421',1000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4705179869058384',3,'200804','202004','6832',15000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4778887665102069',4,'200803','202003','8100',900000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4720999501775726',4,'201002','202202','6812',70000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4315285179011207',5,'201001','202201','9865',15000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4813559786031868',6,'201109','202309','8402',12000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4562482253184232',7,'201110','202310','6035',1000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4394485803687176',8,'201111','202311','9822',20000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4397622918073892',9,'201212','202412','1258',10000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4263913794226636',10,'201209','202409','1035',4500.00,'vigente');
	INSERT INTO tarjeta VALUES ('4641381760773711',11,'201208','202408','2610',5000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4683099390400259',12,'200707','201907','3078',100000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4988131767657095',13,'200612','201812','9534',80000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4674630639638823',14,'200711','201911','8026',30000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4764684936724189',15,'200712','201912','7098',25000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4626337979547995',16,'200612','201812','6812',12000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4619408553568483',17,'200808','202008','5456',10000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4648528090512563',18,'200807','202007','4232',100000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4444701795160093',19,'200801','202001','3589',50000.00,'vigente');
	INSERT INTO tarjeta VALUES ('4901474084361461',20,'200702','201902','2018',500.00,'vigente');
	CREATE FUNCTION cierre_tarjetas () RETURNS void AS $$
		DECLARE
			mes int;
			terminacion int;
			random int;
			fechaInicio date;
			fechaCierre date;
			fechavto date;
			
		BEGIN
			FOR mes in 1..12 LOOP
				FOR terminacion in 0..9 LOOP
					random := FLOOR(RANDOM()*(15)+2);
					fechaInicio := ('2018-' || mes || '-' || random)::date;
					fechaCierre := fechaInicio + INTERVAL '1 month';
					fechavto    := fechaCierre + INTERVAL '15 days';

					INSERT INTO cierre VALUES(2018, mes, terminacion, fechaInicio, fechaCierre, fechavto);
				END LOOP;
			END LOOP;
		END;
	$$ LANGUAGE plpgsql;
`

var eliminar_keys = `alter table tarjeta drop constraint tarjeta_nrocliente_fk;
	alter table compra drop constraint compra_nrotarjeta_fk;
	alter table compra drop constraint compra_nrocomercio_fk;
	alter table rechazo drop constraint rechazo_nrocomercio_fk;
	alter table cabecera drop constraint cabecera_nrotarjeta_fk;

	alter table cliente drop constraint cliente_pk;
	alter table tarjeta drop constraint tarjeta_pk;
	alter table comercio drop constraint comercio_pk;
	alter table compra drop constraint compra_pk;
	alter table rechazo drop constraint rechazo_pk;
	alter table cierre drop constraint cierre_pk;
	alter table cabecera drop constraint cabecera_pk;
	alter table detalle drop constraint detalle_pk;
	alter table alerta drop constraint alerta_pk;
`

var SPautorizar_compra = `CREATE FUNCTION autorizar_compra(numerotarj char(16), codseguridad char(4), nrocomercio int, montoingresado decimal(7,2)) RETURNS boolean AS $$
	DECLARE 

		verificador record;
		monto_total int;
		fecha_tarjeta date;
		fecha_actual date;

	BEGIN
		SELECT * INTO verificador FROM tarjeta WHERE numerotarj = tarjeta.nrotarjeta;
		IF not FOUND THEN
			INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?tarjeta no válida');
			RETURN false;

		ELSE
			IF verificador.estado != 'vigente' THEN
				INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?tarjeta no vigente');
				RETURN false;

			ELSE
				IF verificador.codseguridad != codseguridad THEN
					INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?código de seguridad inválido');
					RETURN false;

				ELSE
					SELECT SUM (monto) INTO monto_total FROM compra WHERE numerotarj = compra.nrotarjeta and compra.pagado = false;
					IF monto_total ISNULL THEN
						monto_total := 0;
					END IF;
					
					IF monto_total + montoingresado > verificador.limitecompra THEN
						INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?supera límite de tarjeta');
						RETURN false;

					ELSE
						fecha_tarjeta := TO_DATE(verificador.validahasta, 'YYYYMM' );
						fecha_actual := NOW()::DATE;
						IF (fecha_actual >= fecha_tarjeta) THEN
							INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, '?plazo de vigencia expirado');
							RETURN false;

						ELSE
							SELECT * INTO verificador FROM tarjeta WHERE tarjeta.estado = 'suspendida';
							IF verificador.estado = 'suspendida' THEN
								INSERT INTO rechazo VALUES (DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, 'la tarjeta se encuentra suspendida');
								RETURN false;

							ELSE
								INSERT INTO compra VALUES(DEFAULT, numerotarj, nrocomercio, now()::DATE, montoingresado, false);
								RETURN true;
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;

	RETURN false;

	END;
	$$ LANGUAGE plpgsql;
`

var SPgenerar_resumen = `CREATE FUNCTION generar_resumen(numerocli int, periodo_año char(4), periodo_mes char(2)) RETURNS void AS $$ 
	DECLARE 

		linea int;
		comercioDet RECORD;
		candidato RECORD;
		t RECORD;
		c RECORD;
		fechas RECORD;

	BEGIN
		SELECT * INTO candidato FROM cliente WHERE numerocli = cliente.nrocliente;

		FOR t IN SELECT * FROM tarjeta WHERE candidato.nrocliente = tarjeta.nrocliente LOOP

			linea := 0;

			SELECT fechainicio,fechacierre,fechavto into fechas from cierre where 
			to_number(RIGHT(t.nrotarjeta, 1),'0') = cierre.terminacion and 
			to_number(periodo_año,'9999') = cierre.año and to_number(periodo_mes, '99') = cierre.mes;

			INSERT INTO cabecera VALUES (DEFAULT, candidato.nombre, candidato.apellido, candidato.direccion, t.nrotarjeta, fechas.fechainicio, fechas.fechacierre, fechas.fechavto);

			FOR c IN SELECT * FROM compra WHERE t.nrotarjeta = compra.nrotarjeta AND (compra.fecha >= fechas.fechainicio AND compra.fecha <= fechas.fechacierre) LOOP

				linea = linea + 1;
				SELECT * INTO comercioDet FROM comercio WHERE c.nrocomercio = comercio.nrocomercio;

				INSERT INTO detalle VALUES( (SELECT MAX(nroresumen) FROM cabecera) , linea, c.fecha, comercioDet.nombre, c.monto);

			END LOOP;

			UPDATE cabecera SET total = (SELECT COUNT(monto) FROM detalle WHERE detalle.nroresumen = (SELECT MAX(nroresumen) FROM cabecera))
			WHERE cabecera.nroresumen = (SELECT MAX(nroresumen) FROM cabecera);

		END LOOP;
	END;
	$$ LANGUAGE plpgsql;
`

var trigger_compra_rechazada = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_rechazada() RETURNS TRIGGER AS $$
	BEGIN
		INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), new.nrorechazo, 0, 'rechazo');
		RETURN new;
	END;
	$$ LANGUAGE plpgsql;

	CREATE TRIGGER trigger_compra_rechazada 
	AFTER 
	INSERT ON rechazo
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_rechazada();
`

var trigger_compra_unminuto = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_unminuto() RETURNS TRIGGER AS $$
	DECLARE
		anterior record;
		codpostal_nueva record;
		codpostal_anterior record;
		
	BEGIN

		SELECT * INTO anterior FROM compra WHERE new.nrotarjeta = compra.nrotarjeta and 
		compra.nrooperacion = (SELECT MAX (nrooperacion) FROM compra);

		SELECT codigopostal INTO codpostal_nueva FROM comercio WHERE new.nrocomercio = comercio.nrocomercio;
		SELECT codigopostal INTO codpostal_anterior FROM comercio WHERE anterior.nrocomercio = comercio.nrocomercio;

		IF FOUND THEN
			IF (((new.fecha - anterior.fecha) < INTERVAL '1 min')) and (codpostal_nueva = codpostal_anterior) THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), NULL, 1, 'compra 1min');
			END IF;
		END IF;

		RETURN new;

	END;

	$$ LANGUAGE plpgsql;

	CREATE TRIGGER trigger_compra_unminuto
	BEFORE
	INSERT ON compra
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_unminuto();
`

var trigger_compra_cincominutos = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_cincominutos() RETURNS TRIGGER AS $$
	DECLARE

		anterior record;
		codpostal_nueva record;
		codpostal_anterior record;
		
	BEGIN

		SELECT * INTO anterior FROM compra WHERE new.nrotarjeta = compra.nrotarjeta and 
		compra.nrooperacion = (SELECT MAX (nrooperacion) FROM compra);

		SELECT codigopostal INTO codpostal_nueva FROM comercio WHERE new.nrocomercio = comercio.nrocomercio;
		SELECT codigopostal INTO codpostal_anterior FROM comercio WHERE anterior.nrocomercio = comercio.nrocomercio;

		IF FOUND THEN
			IF (((new.fecha - anterior.fecha) < INTERVAL '5 mins')) and (codpostal_nueva != codpostal_anterior) THEN
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), NULL, 5, 'compra 5min');
			END IF;
		END IF;

		RETURN new;

	END;

	$$ LANGUAGE plpgsql;

	CREATE TRIGGER trigger_compra_cincominutos
	BEFORE
	INSERT ON compra
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_cincominutos();
`

var trigger_compra_dosrechazos = `CREATE OR REPLACE FUNCTION funcion_trigger_compra_dosrechazos() RETURNS TRIGGER AS $$
	DECLARE

		rechazos INT;

	BEGIN
		IF new.motivo = '?supera límite de tarjeta' THEN
			SELECT COUNT (*) INTO rechazos FROM rechazo WHERE new.nrotarjeta = rechazo.nrotarjeta and 
															new.fecha::date = rechazo.fecha::date
															and rechazo.motivo = '?supera límite de tarjeta';
			IF rechazos = 2 THEN
				UPDATE tarjeta SET estado = 'suspendida' WHERE new.nrotarjeta = tarjeta.nrotarjeta;
				
				INSERT INTO alerta VALUES(DEFAULT, new.nrotarjeta, now(), new.nrorechazo, 32, 'limite');
			END IF;
		END IF;

		RETURN new;
	END;

	$$ LANGUAGE plpgsql;

	CREATE TRIGGER trigger_compra_dosrechazos
	AFTER
	INSERT ON rechazo
	FOR EACH ROW
	EXECUTE PROCEDURE funcion_trigger_compra_dosrechazos();
`

var crear_consumos = `INSERT INTO consumo VALUES ('4627633796432790', '1234', 1, 5000);
	INSERT INTO consumo VALUES ('4606669094765287', '9825', 2, 90000);
	INSERT INTO consumo VALUES ('4606669094765288', '9825', 2, 90000);
	INSERT INTO consumo VALUES ('4309892964905889', '5421', 3, 1001);
	INSERT INTO consumo VALUES ('4309892964905889', '5421', 4, 1000);
`

var probar_consumo = `CREATE FUNCTION prueba_consumo() RETURNS void AS $$
	DECLARE
		c RECORD;
		autorizado boolean;

	BEGIN
		FOR c IN (SELECT * FROM consumo) LOOP
			SELECT autorizar_compra(c.nrotarjeta, c.codseguridad, c.nrocomercio, c.monto) INTO autorizado;
		END LOOP;

	END;
	$$ LANGUAGE plpgsql;
`

var ejecutar_consumo = `SELECT prueba_consumo();
`

func createDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	db.Exec("drop database banco")
	_, err = db.Exec(`create database banco`)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()
}

func iniciar_tablas(db *sql.DB) {
	_, err := db.Exec(crear_tablas)
	if err != nil {
		log.Fatal(err)
	}
}

func iniciar_keys(db *sql.DB) {
	_, err := db.Exec(crear_keys)
	if err != nil {
		log.Fatal(err)
	}
}

func iniciar_datos(db *sql.DB) {
	_, err := db.Exec(crear_datos)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(crear_consumos)
	if err != nil {
		log.Fatal(err)
	}
}

func borrar_keys(db *sql.DB) {
	_, err := db.Exec(eliminar_keys)
	if err != nil {
		log.Fatal(err)
	}
}

func autorizar_compra(db *sql.DB) {
	_, err := db.Exec(SPautorizar_compra)
	if err != nil {
		log.Fatal(err)
	}
}

func generar_resumen(db *sql.DB) {
	_, err := db.Exec(SPgenerar_resumen)
	if err != nil {
		log.Fatal(err)
	}
}

func funciones(db *sql.DB) {
	_, err := db.Exec(SPautorizar_compra)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(SPgenerar_resumen)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(probar_consumo)
	if err != nil {
		log.Fatal(err)
	}
}

func iniciar_consumos(db *sql.DB) {
	_, err := db.Exec(ejecutar_consumo)
	if err != nil {
		log.Fatal(err)
	}
}

func triggers(db *sql.DB) {
	_, err := db.Exec(trigger_compra_rechazada)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_unminuto)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_cincominutos)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(trigger_compra_dosrechazos)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	createDatabase()

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=banco sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var opcionElegida string

	check := 0

	for check < 1 {
		fmt.Printf("1. Crear tablas \n")
		fmt.Printf("2. Crear PK y FK \n")
		fmt.Printf("3. Insertar datos a las tablas \n")
		fmt.Printf("4. Eliminar PK y FK de las tablas \n")
		fmt.Printf("5. Crear funciones \n")
		fmt.Printf("6. Crear triggers \n")
		fmt.Printf("7. Probar consumo \n")
		fmt.Printf("0. Salir. \n")

		fmt.Printf("\nOpcion elegida: ")
		fmt.Scanf("%s", &opcionElegida)

		if opcionElegida == "1" {
			iniciar_tablas(db)
			fmt.Printf("Tablas creadas. \n\n")
		} else if opcionElegida == "2" {
			iniciar_keys(db)
			fmt.Printf("PK y FK agregadas a las tablas. \n\n")
		} else if opcionElegida == "3" {
			iniciar_datos(db)
			fmt.Printf("Datos ingresados a las tablas. \n\n")
		} else if opcionElegida == "4" {
			borrar_keys(db)
			fmt.Printf("PK y FK borradas de las tablas. \n\n")
		} else if opcionElegida == "5" {
			funciones(db)
			fmt.Printf("Funciones creadas. \n\n")
		} else if opcionElegida == "6" {
			triggers(db)
			fmt.Printf("Triggers creados. \n\n")
		} else if opcionElegida == "7" {
			iniciar_consumos(db)
			fmt.Printf("Consumos Testeados, verificar en la base de datos. \n\n")
		} else if opcionElegida == "0" {
			fmt.Printf("Gracias. Vuelva prontos! \n\n")
			check = 1
		} else {
			fmt.Printf("Opcion incorrecta! Elija nuevamente por favor.\n\n")
		}
	}
}
