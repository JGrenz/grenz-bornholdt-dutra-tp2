package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	bolt "github.com/coreos/bbolt"
)

type Cliente struct {
	Nrocliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}
type Tarjeta struct {
	Nrotarjeta   int
	Nrocliente   int
	Validadesde  string
	Validahasta  string
	Codseguridad string
	Limitecompra float64
	Estado       string
}
type Comercio struct {
	Nrocomercio  int
	Nombre       string
	Domicilio    string
	Codigopostal string
	Telefono     string
}
type Compra struct {
	Nrooperacion int
	Nrotarjeta   int
	Nrocomercio  int
	Fecha        time.Time
	Monto        float64
	Pagado       bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {
	var opcionElegida string
	check := 0

	for check < 1 {
		fmt.Printf("1. Clientes NoSQL \n")
		fmt.Printf("2. Tarjetas NoSQL \n")
		fmt.Printf("3. Comercios NoSQL \n")
		fmt.Printf("4. Compras NoSQL \n")
		fmt.Printf("0. Salir. \n")

		fmt.Printf("\nOpcion elegida: ")
		fmt.Scanf("%s", &opcionElegida)

		if opcionElegida == "1" {
			clientes()
			fmt.Printf("Tabla Clientes cargada! \n\n")
		} else if opcionElegida == "2" {
			tarjetas()
			fmt.Printf("Tabla Tarjetas cargada! \n\n")
		} else if opcionElegida == "3" {
			comercios()
			fmt.Printf("Tabla Comercios cargada! \n\n")
		} else if opcionElegida == "4" {
			compras()
			fmt.Printf("Tabla Compras cargada! \n\n")
		} else if opcionElegida == "0" {
			fmt.Printf("Gracias. Vuelva prontos! \n\n")
			check = 1
		} else {
			fmt.Printf("Opcion incorrecta! Elija nuevamente por favor.\n\n")
		}
	}

}

func clientes() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	cliente := Cliente{8, "Jack", "Sparrow", "AV. LISANDRO DE LA TORRE 1151", "15-6330-6314"}
	data, err := json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)

	cliente = Cliente{13, "Homero", "Simpson", "AV. SIEMPREVIVA 742", "15-7761-1995"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)

	cliente = Cliente{18, "Luke", "Skywalker", "PARANÁ 6370", "15-7466-9047"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))
	fmt.Printf("%s\n", resultado)
}

func tarjetas() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tarjeta := Tarjeta{4394485803687176, 8, "201111", "202311", "9822", 20000.00, "vigente"}
	data, err := json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err := ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)

	tarjeta = Tarjeta{4988131767657095, 13, "200612", "201812", "9534", 80000.00, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)

	tarjeta = Tarjeta{4648528090512563, 18, "200807", "202007", "4232", 100000.00, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))
	fmt.Printf("%s\n", resultado)
}

func comercios() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	comercio := Comercio{3, "Hamburguesas Ketchup", "Romian 510", "B6450XAB", "15-4741-2997"}
	data, err := json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

	comercio = Comercio{12, "Sorny", "9 de Julio 907", "B8164XAD", "15-6870-9934"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

	comercio = Comercio{13, "Fabrega", "25 de Mayo 2505", "B7530XAC", "15-9298-8123"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))
	fmt.Printf("%s\n", resultado)

}

func compras() {
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	t := time.Date(2018, time.July, 3, 16, 30, 0, 0, time.UTC)
	compra := Compra{1, 4394485803687176, 3, t.Local(), 1500.00, true}
	data, err := json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err := ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

	t = time.Date(2018, time.October, 26, 22, 15, 35, 0, time.UTC)
	compra = Compra{2, 4394485803687176, 12, t.Local(), 240.50, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

	t = time.Date(2018, time.May, 13, 9, 45, 21, 0, time.UTC)
	compra = Compra{3, 4394485803687176, 13, t.Local(), 600.50, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
	fmt.Printf("%s\n", resultado)

}
